package test.app.imagegallerytest.model;

import java.io.Serializable;
import java.util.List;

public class GalleryResponse implements Serializable {

    public List<GalleryObject> hits;

}
