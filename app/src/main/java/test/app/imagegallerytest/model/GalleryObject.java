package test.app.imagegallerytest.model;

import java.io.Serializable;

public class GalleryObject implements Serializable {

    public String largeImageURL;

    public int previewWidth;

    public int previewHeight;

    public String previewURL;

}
