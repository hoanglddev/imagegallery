package test.app.imagegallerytest.ui;

import java.util.HashMap;

import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;
import test.app.imagegallerytest.network.ServerTask;

public class MainPresenter {

    private MainView view;

    public MainPresenter(MainView view) {
        this.view = view;
    }

    public void getGallery(int page) {

        HashMap<String, String> params = new HashMap<>();

        params.put("key", "10961674-bf47eb00b05f514cdd08f6e11");
        params.put("page", String.valueOf(page));

        ServerTask.getInstance().getServices().getGallery(params)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(response -> {
                    view.onGetGallerySuccess(response);
                }, err -> {
                    view.onGetGalleryFailed(err);
                });

    }

}
