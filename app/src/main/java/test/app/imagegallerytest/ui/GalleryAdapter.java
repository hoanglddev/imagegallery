package test.app.imagegallerytest.ui;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.constraint.ConstraintLayout;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.bumptech.glide.Glide;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import test.app.imagegallerytest.R;
import test.app.imagegallerytest.model.GalleryObject;

public class GalleryAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private boolean isLoadmore;

    public interface GalleryAdapterListener {
        void onGalleryItemClick(int position, GalleryObject galleryObject);
        void onLoadmore();
    }

    private List<GalleryObject> galleryObjects;
    private Context context;
    private RecyclerView recyclerView;
    private GalleryAdapterListener listener;

    public GalleryAdapter(List<GalleryObject> galleryObjects, GalleryAdapterListener listener) {
        this.galleryObjects = galleryObjects;
        this.listener = listener;
    }

    @Override
    public void onAttachedToRecyclerView(@NonNull RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
        this.recyclerView = recyclerView;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        context = viewGroup.getContext();
        View view = LayoutInflater.from(context).inflate(R.layout.gallery_item, viewGroup, false);
        return new GalleryHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder viewHolder, int position) {
        ((GalleryHolder) viewHolder).bindData(position, galleryObjects.get(position));
    }

    public void setData(List<GalleryObject> galleryObjects) {
        this.galleryObjects = galleryObjects;
        notifyDataSetChanged();
    }

    public List<GalleryObject> getData() {
        return this.galleryObjects;
    }

    public void addAllData(List<GalleryObject> galleryObjects) {
        this.galleryObjects.addAll(galleryObjects);
        notifyDataSetChanged();
    }

    public void setLoadMore(boolean isLoadmore) {
        this.isLoadmore = isLoadmore;
        notifyDataSetChanged();
    }

    public boolean isLoadmore() {
        return this.isLoadmore;
    }

    @Override
    public int getItemCount() {
        return galleryObjects.size();
    }

    public class GalleryHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.gallery_item_im)
        ImageView imGallery;

        @BindView(R.id.gallery_item_container)
        ConstraintLayout layoutContainer;

        @BindView(R.id.gallery_item_btn_loadmore)
        View btnLoadmore;

        private int position;
        private GalleryObject galleryObject;

        public GalleryHolder(@NonNull View itemView) {
            super(itemView);

            ButterKnife.bind(this, itemView);
        }

        public void bindData(int position, GalleryObject galleryObject) {
            if (isLoadmore && galleryObject == null) {
                btnLoadmore.setVisibility(View.VISIBLE);
                imGallery.setVisibility(View.GONE);
                return;
            }

            this.position = position;
            this.galleryObject = galleryObject;

            btnLoadmore.setVisibility(View.GONE);
            imGallery.setVisibility(View.VISIBLE);

            StaggeredGridLayoutManager.LayoutParams layoutParams =
                    (StaggeredGridLayoutManager.LayoutParams) layoutContainer.getLayoutParams();

            layoutParams.width = galleryObject.previewWidth + 20;
            layoutParams.height = galleryObject.previewWidth + 20;
            layoutParams.rightMargin = 20;
            layoutParams.topMargin = 20;
            recyclerView.requestLayout();

            Glide.with(context).load(galleryObject.previewURL)
                    .into(imGallery);
        }

        @OnClick(R.id.gallery_item_container)
        public void actionItemClick() {
            listener.onGalleryItemClick(position, galleryObject);
        }

        @OnClick(R.id.gallery_item_btn_loadmore)
        public void actionBtnLoadMore() {
            listener.onLoadmore();
        }
    }
}
