package test.app.imagegallerytest.ui;

import test.app.imagegallerytest.model.GalleryResponse;

public interface MainView {

    void onGetGallerySuccess(GalleryResponse response);

    void onGetGalleryFailed(Throwable err);

}
