package test.app.imagegallerytest.ui;

import android.Manifest;
import android.app.AlertDialog;
import android.app.DownloadManager;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.content.Context;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Color;
import android.graphics.Point;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.Message;
import android.support.annotation.NonNull;
import android.support.annotation.RequiresApi;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.NotificationCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;

import com.bumptech.glide.Glide;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import test.app.imagegallerytest.R;
import test.app.imagegallerytest.model.GalleryObject;
import test.app.imagegallerytest.model.GalleryResponse;



public class MainActivity extends AppCompatActivity implements MainView, GalleryAdapter.GalleryAdapterListener {

    @BindView(R.id.main_activity_rv_main)
    RecyclerView rvMain;

    private GalleryAdapter adapter;
    private MainPresenter presenter;
    private NotificationManager notificationManager;

    final String CHANNEL_ID = "my_channel_01";
    final int NOTIFICATION_ID = 234;
    private int currentPage;

    public final static int REQUEST_PERMISSION_WRITE_EXTERNAL_STORAGE = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        ButterKnife.bind(this);

        adapter = new GalleryAdapter(new ArrayList<>(), this);
        adapter.setLoadMore(true);
        rvMain.setAdapter(adapter);

        currentPage = 1;
        presenter = new MainPresenter(this);
        presenter.getGallery(currentPage);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (!isHavePermission()) {
                Toast.makeText(this, "App cannot download gallery resource", Toast.LENGTH_SHORT).show();
            }
        }
    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    private boolean isHavePermission() {
        int hasPermission = ContextCompat.checkSelfPermission(MainActivity.this, Manifest.permission.WRITE_EXTERNAL_STORAGE);
        if (hasPermission != PackageManager.PERMISSION_GRANTED) {
            if (shouldShowRequestPermissionRationale(Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
                Toast.makeText(this, "You need to allow write external storage permission." +
                        "\nIf you disable this permission, You will not able to use app.", Toast.LENGTH_SHORT).show();
            } else {
                ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, REQUEST_PERMISSION_WRITE_EXTERNAL_STORAGE);
            }
            return false;
        }

        return true;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        switch (requestCode) {
            case REQUEST_PERMISSION_WRITE_EXTERNAL_STORAGE :

                if (grantResults.length == 0
                        || grantResults[0] != PackageManager.PERMISSION_GRANTED) {
                    Toast.makeText(MainActivity.this, "Permission denied to write your External storage", Toast.LENGTH_SHORT).show();
                    ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, 0);
                }
                return;
        }
    }

    private void updateLayoutForGallery(GalleryObject galleryObject) {

        Display display = getWindowManager(). getDefaultDisplay();
        Point size = new Point();
        display. getSize(size);
        int width = size.x - 100;
        int numCol;

        if (width/galleryObject.previewWidth <= 2)
            numCol = 2;
        else
            numCol = width/galleryObject.previewWidth;

        StaggeredGridLayoutManager layoutManager = new StaggeredGridLayoutManager(numCol,
                StaggeredGridLayoutManager.VERTICAL);
        rvMain.setLayoutManager(layoutManager);
    }

    @Override
    public void onGetGallerySuccess(GalleryResponse response) {
        if (response == null || response.hits == null || response.hits.isEmpty()) {
            adapter.setLoadMore(false);
            return;
        }

        List<GalleryObject> galleries = response.hits;

        galleries.add(null); // add loadmore item

        updateLayoutForGallery(response.hits.get(0));
        adapter.addAllData(response.hits);
    }

    @Override
    public void onGetGalleryFailed(Throwable err) {

    }

    public void showDialogGallery(GalleryObject galleryObject) {
        if (galleryObject == null) return;

        AlertDialog.Builder builder = new AlertDialog.Builder(this);

        View view = LayoutInflater.from(this).inflate(R.layout.dialog_gallery, null);
        ImageView imGallery = view.findViewById(R.id.dialog_gallery_im_gallery);
        Button btnDownload = view.findViewById(R.id.dialog_gallery_btn_download);

        Glide.with(this).load(galleryObject.largeImageURL).into(imGallery);

        btnDownload.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                downloadGallery(galleryObject);
            }
        });

        builder.setView(view);
        AlertDialog dialog = builder.create();
        dialog.show();
    }

    private void downloadGallery(GalleryObject galleryObject) {
        String fileName = String.valueOf(System.currentTimeMillis()) +
                galleryObject.largeImageURL.substring(galleryObject.largeImageURL.lastIndexOf("."));

        DownloadManager.Request request = new DownloadManager.Request(Uri.parse(galleryObject.largeImageURL));
        request.setDescription("Photo");
        request.setTitle(fileName);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
            request.allowScanningByMediaScanner();
            request.setNotificationVisibility(DownloadManager.Request.VISIBILITY_VISIBLE_NOTIFY_COMPLETED);
            request.setNotificationVisibility(DownloadManager.Request.VISIBILITY_VISIBLE_NOTIFY_ONLY_COMPLETION);
        }
        request.setDestinationInExternalPublicDir(Environment.DIRECTORY_DOWNLOADS, fileName);

        DownloadManager manager = (DownloadManager) getSystemService(Context.DOWNLOAD_SERVICE);
        final long downloadId = manager.enqueue(request);

        final int UPDATE_PROGRESS = 5020;

        notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
            String CHANNEL_ID = "my_channel_01";
            CharSequence name = "my_channel";
            String Description = "This is my channel";
            int importance = NotificationManager.IMPORTANCE_DEFAULT;
            NotificationChannel mChannel = new NotificationChannel(CHANNEL_ID, name, importance);
            mChannel.setDescription(Description);
            mChannel.enableLights(true);
            mChannel.setLightColor(Color.RED);
            mChannel.enableVibration(true);
            mChannel.setVibrationPattern(new long[]{100, 200, 300, 400, 500, 400, 300, 200, 400});
            mChannel.setShowBadge(false);
            notificationManager.createNotificationChannel(mChannel);
        }

        NotificationCompat.Builder builder = new NotificationCompat.Builder(this, CHANNEL_ID)
                .setSmallIcon(R.mipmap.ic_launcher)
                .setContentTitle("Download")
                .setContentText("Download");

        final Handler handler = new Handler() {
            @Override
            public void handleMessage(Message msg) {
                if(msg.what==UPDATE_PROGRESS) {
                    double downloaded = (double)((msg.arg1)/1024)/1024;
                    double total = (double) ((msg.arg2)/1024)/1024;
//                    String downloaded = String.format("%.2f MB", (double)((msg.arg1)/1024)/1024);
//                    String total = String.format("%.2f MB", (double) ((msg.arg2)/1024)/1024);
                    int progress = (int) (downloaded/total * 100);
//                    pDialog.setTitleText(status);

                    builder.setContentText(progress + "%");
                    builder.setProgress(100, progress, false);
                    notificationManager.notify(NOTIFICATION_ID, builder.build());

                    if (progress >= 99) {
                        builder.setContentText("Download complete")
                                // Removes the progress bar
                                .setProgress(0,0,false);
                        notificationManager.notify(NOTIFICATION_ID, builder.build());
                    }
                }
                super.handleMessage(msg);
            }
        };
        new Thread(new Runnable() {
            @Override
            public void run() {
                boolean downloading = true;
                while (downloading) {
                    DownloadManager.Query q = new DownloadManager.Query();
                    q.setFilterById(downloadId);
                    Cursor cursor = manager.query(q);
                    cursor.moveToFirst();
                    int bytes_downloaded = cursor.getInt(cursor.getColumnIndex(DownloadManager.COLUMN_BYTES_DOWNLOADED_SO_FAR));
                    int bytes_total = cursor.getInt(cursor.getColumnIndex(DownloadManager.COLUMN_TOTAL_SIZE_BYTES));
                    if (cursor.getInt(cursor.getColumnIndex(DownloadManager.COLUMN_STATUS)) == DownloadManager.STATUS_SUCCESSFUL) {
                        downloading = false;
                    }

                    //Post message to UI Thread
                    Message msg = handler.obtainMessage();
                    msg.what = UPDATE_PROGRESS;
                    //msg.obj = statusMessage(cursor);
                    msg.arg1 = bytes_downloaded;
                    msg.arg2 = bytes_total;
                    handler.sendMessage(msg);
                    cursor.close();
                }
            }
        }).start();
    }

    @Override
    public void onGalleryItemClick(int position, GalleryObject galleryObject) {
        showDialogGallery(galleryObject);
    }

    @Override
    public void onLoadmore() {
        if (!adapter.isLoadmore()) return;

        if (!adapter.getData().isEmpty()) {
            adapter.getData().remove(adapter.getItemCount() - 1); // remove item loadmore
        }

        currentPage++;
        presenter.getGallery(currentPage);
    }
}
