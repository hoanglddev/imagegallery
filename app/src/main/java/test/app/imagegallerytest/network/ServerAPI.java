package test.app.imagegallerytest.network;

import java.util.Map;

import retrofit2.http.GET;
import retrofit2.http.QueryMap;
import test.app.imagegallerytest.model.GalleryResponse;

public interface ServerAPI {

//    api/?key=10961674-bf47eb00b05f514cdd08f6e11&page=1

    @GET("/api")
    rx.Observable<GalleryResponse> getGallery(@QueryMap Map<String, String> params);

}
